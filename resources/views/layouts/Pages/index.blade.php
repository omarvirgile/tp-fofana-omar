@extends('layouts.main')
@section('content')
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Liste des Pays <a href="{{ route('lands.create') }}"><button class="btn btn-warning btn-circle btn-lg" type="button">+</button></a></h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 300px;">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Libelle</th>
                      <th>Description</th>
                      <th>Code Indicatif</th>
                      <th>Continent</th>
                      <th>Population</th>
                      <th>Capitale</th>
                      <th>Monnaie</th>
                      <th>Langue</th>
                      <th>Superficie</th>
                      <th>est_laique</th>
                      
                     </tr>
                  </thead>
                  <tbody>
                    @foreach($lands as $land)
                      <tr>
                          <td>{{$land['id']}}</td>
                          <td>{{$land['libelle']}}</td>
                          <td>{{$land['description']}}</td>
                          <td>{{$land['code_indicatif']}}</td>
                          <td>{{$land['continent']}}</td>
                          <td>{{$land['population']}}</td>
                          <td>{{$land['capitale']}}</td>
                          <td>{{$land['monnaie']}}</td>
                          <td>{{$land['langue']}}</td>
                          <td>{{$land['superficie']}}</td>
                          <td>{{$land['est_laique']}}</td>
                          <td>
                          <a href="{{ route('lands.show', ["id" => $land->id]) }}"><button class="btn btn-success btn-circle" type="button"><i class="fa fa-eye"></i></button>
                          <a href="{{ route('lands.edit', ["id" => $land->id]) }}"><button class="btn btn-primary btn-circle" type="button"><i class="fa fa-pen"></i></button>
                          <a href="{{ route('lands.destroy', ["id" => $land->id]) }}"><button class="btn btn-danger btn-circle" type="button"><i class="fa fa-trash"></i></button>
                          </td>
                        </tr>
                    @endforeach
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          
@endsection