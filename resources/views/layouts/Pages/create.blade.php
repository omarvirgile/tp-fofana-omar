@extends('layouts.main')
@section('content')
<div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Ajout D'un Pays</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form method="POST" action=' {{ route("lands.store") }} '>
                  @csrf
                  @method('POST')
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Libelle</label>
                        <input type="text" class="form-control" name="libelle" required placeholder="Entrer le libelle">
                      </div>
                    </div>
                   
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- textarea -->
                      <div class="form-group">
                        <label >Code indicatif</label>
                        <input type="text" class="form-control " required name="code_indicatif" placeholder="Entrer le code indicatif">
                      </div>
                    </div>
                   
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- textarea -->
                      <div class="form-group">
                        <label >Capitale</label>
                        <input type="text" class="form-control " required name="capitale" placeholder="Entrer la capitale du pays">
                      </div>
                    </div>
                   
                  </div>
                 <div class="row">
                    <div class="col-sm-2">
                      <!-- select -->
                      <div class="form-group">
                        <label >Continent</label>
                        
                        <select  name="continent">
                          <option>Afrique</option>
                          <option>Europe</option>
                          <option>Amérique</option>
                          <option>Asie</option>
                          <option>Océanie</option>
                          <option>Antarticque</option>
                        </select>
                      </div>
                    </div>
                   
                  </div>

                  <div class="row">
                    <div class="col-sm-1">
                      <!-- select -->
                      <div class="form-group">
                        <label >Langue</label>
                        
                        <select class="form-control" name="langue">
                          <option>EN</option>
                          <option>FR</option>
                          <option>AR</option>
                          <option>ES</option>
                         
                        </select>
                      </div>
                    </div>
                   
                  </div>

                  <div class="row">
                    <div class="col-sm-1">
                      <!-- select -->
                      <div class="form-group">
                        <label >Monnaie</label>
                        
                        <select name="monnaie">
                          <option>XOF</option>
                          <option>EUR</option>
                          <option>DOLLAR</option>
                         
                        </select>
                      </div>
                    </div>
                   
                  </div>

                  <div class="row">
                    <div class="col-sm-1">
                      <!-- select -->
                      <div class="form-group">
                        <label >Est_Laique</label>
                        
                        <select name="est_laique">
                          <option>0</option>
                          <option>1</option>
                          
                         
                        </select>
                      </div>
                    </div>
                   
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <!-- textarea -->
                      <div class="form-group">
                        <label >Populations</label>
                        <input type="text" class="form-control " required name="population" placeholder="Entrer le nombre d'habitants" >
                      </div>
                    </div>
                   
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <!-- textarea -->
                      <div class="form-group">
                        <label >Superficie</label>
                        <input type="text" class="form-control " required name="superficie" placeholder="Entrer la superficie en km²" required>
                      </div>
                    </div>



                   
                  </div>

                  

                  
                  <div class="row">
                    <div class="col-lg">
                      <!-- textarea -->
                      <div class="form-group">
                        <label >Description</label>
                        <textarea class="form-control" rows="5" name="description" placeholder="Entrer une descritption du pays"></textarea>
                      </div>
                    </div>
                    
                   
                  </div>


               
                  <div class="card-footer">
                      <button type="submit" class="btn btn-warning">Ajouter</button>
                    </div>

                
                  
                </form>
                
             

@endsection