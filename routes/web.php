<?php


use App\Http\Controllers\LandController;
use App\Models\Land;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::view('/', 'layouts.main');
Route::view('/index', 'layouts.Pages.index');
Route::view('/main', 'layouts.Pages.main1');
Route::get('/index', [LandController::class, 'index'])->name('lands.index');
Route::get('/index/create', [LandController::class, 'create'])->name('lands.create');
Route::get('/index/{id}', [LandController::class, 'show'])->name('lands.show');
Route::post('/index', [LandController::class, 'store'])->name('lands.store');
Route::get('/index/{id}/edit', [LandController::class, 'edit'])->name('lands.edit');
Route::post('/index/{id}/update', [LandController::class, 'update'])->name('lands.update');
Route::get('/index/{id}/destroy', [LandController::class, 'destroy'])->name('lands.destroy');