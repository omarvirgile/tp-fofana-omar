<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Land;


class Landcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.Pages.index', ['lands' => Land::all()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.Pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "libelle"=>"required",
            "description"=>"required",
            "code_indicatif"=>"required",
            "continent"=>"required",
            "population"=>"required",
            "capitale"=>"required",
            "monnaie"=>"required",
            "langue"=>"required",
            "superficie"=>"required",
            "est_laique"=>"required",
        ]);
        //Enregistrement de User dans la Base de données
        Land::create([
            "libelle"=>$request->get("libelle"),
            "description"=>$request->get("description"),
            "code_indicatif"=>$request->get("code_indicatif"),
            "continent"=>$request->get("continent"),
            "population"=>$request->get("population"). " hbts ",
            "capitale"=>$request->get("capitale"),
            "monnaie"=>$request->get("monnaie"),
            "langue"=>$request->get("langue"),
            "superficie"=>$request->get("superficie"). " km² ",
            "est_laique"=>$request->get("est_laique"),
            
            
            
        ]);

        return redirect()->route('lands.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $land = Land::findOrFail($id);
        return view("layouts.Pages.show", ['lands' => $land]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $land = Land::findOrFail($id);
        return view("layouts.Pages.edit", ['lands' => $land]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
            "libelle"=>"required",
            "description"=>"required",
            "code_indicatif"=>"required",
            "continent"=>"required",
            "population"=>"required",
            "capitale"=>"required",
            "monnaie"=>"required",
            "langue"=>"required",
            "superficie"=>"required",
            "est_laique"=>"required",
           
        ]);
    
        Land::whereId($id)->update($validatedData);
    
        return redirect('/index')->with('success', 'Enregistrement modifié avec succèss');
    }
        
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $land = Land::findOrFail($id);
         $land->delete();

         return redirect('/index')->with('success', 'Enregistrement supprimé avec succèss');
    }
}
