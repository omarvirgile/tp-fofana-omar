<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $continent=array('Afrique','Amerique', 'Asie', 'Europe', 'Oceanie', 'Antarticque');
        $monnaie=array('XOF','EUR','DOLLAR');
        $langue=array('FR','EN','AR','ES');
        return [
            'libelle'=>$this->faker->country(),
            'description'=>$this->faker->paragraph(),
            'code_indicatif'=>$this->faker->numerify('+###'),
            'continent'=> $continent[array_rand($continent)],
            'population'=>rand(1000000, 1400000000)." hbts ",
            'capitale'=>$this->faker->city(),
            'monnaie'=> $monnaie[array_rand($monnaie)],
            'langue'=> $langue[array_rand($langue)],
            'superficie'=>rand(1, 1400000000)." Km² ",
            'est_laique'=>$this->faker->boolean(),
        ];
    }
}
